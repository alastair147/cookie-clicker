This is a clone of Cookie Clicker - But as a base for a new game.
Credit below.


==============

cookie-clicker
==============

This is a copy of the publicly available source of the game cookie clicker by orteil.  

Here is the actual game:
http://orteil.dashnet.org/cookieclicker/

Here is the orteil home page:
http://orteil.dashnet.org/


The purpose of this repository is to provide a jumping off point for javascript programming for Coder Dojo members.

==============
